;1

(def k15 (take 12 (shuffle (range -30 30))))
(def k16 (take 12 (shuffle (range -30 30))))

(defn avg [n] (/ (reduce + n) (count n)))
(defn keskilamp [k1 k2]
  (float (/ (+ k1 k2) 2)))

(defn keski [f k1 k2]
  (map f k1 k2))

(avg (remove neg? (keski keskilamp k15 k16)))

;2
 (def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}]) 
 
(reduce +  (map
             #(- (% :neste) (% :vesi))
             (filter #(= (% :kk) 4) food-journal)))

;3
 (def food-journal2
   (vec (map
          #(merge (select-keys % [:kk :paiva]) {:muuneste (format "%.2f" (- (% :neste) (% :vesi)))})
          (filter #(= (% :kk) 4) food-journal))))