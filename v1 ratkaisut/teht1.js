
//1)
const merkkijono = m => m.length < 2 ||
    (m.slice(0,1) == m.slice(m.length-1) ?
    merkkijono(m.slice(1,m.length-1)) : false)
//2)    
const syt = (a,b) =>(!b)?a:syt(b,a%b)
//3)
const kjl = (a,b) =>syt(a,b)==1
//4)
const potenssi = (x,n) => n!=1?x*potenssi(x,n-1):x
//5)
const kaanna = function(l,i=0){
  apu = l[i]
  imax = l.length-i-1
  if(i < imax){
    console.log(l)
    l[i] = l[imax]
    l[imax] = apu
    i +=1
    return kaanna(l,i)
  }else{
    return l
  }
}

//Käsittääkseni listan valmiiden operaatioiden käyttö oli kielletty, mutta alla on ratkaisu pop ja push operaatioita käyttämällä.
/*const kaanna = function(a){
  b = []
  r = function(a){
    if(a.length != 0){
      b.push(a.pop())
      r(a)
    }
  }
  r(a)
  return b
}*/
