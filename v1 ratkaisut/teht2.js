

//1
const f =(a,b)=>(
	(()=>(
		a>b)?1:((a<b)?-1:((a==b)?0:null))
	)()
)

//2
const hLampo15 = [-20,-17,-7,3,15,15,19,27,13,14,4,0]
const hLampo16 = [-10,-12,-7,3,15,15,9,22,13,12,4,-1]

const lampotilaVertaa = (v,hLampo15,hLampo16)=>{
    return function vertaa(i=0){
      return (i<hLampo15.length)?(
				v(hLampo15[i],hLampo16[i])==-1)?1+vertaa(i+1):0+(vertaa(i+1)
			):0
    }()
}

//3
const offset = [1,2];
const zoom = 2;
const point = { x: 1, y: 1};
const pipeline  = [   // 2D-muunnoksia
    function translate(p){
        return { x: p.x + offset[0], y: p.y + offset[1] };
    },

    function scale(p){
        return { x: p.x * zoom, y: p.y * zoom};
    },
    function rotation(p){
      return{x: p.x*Math.cos(Math.PI)-p.y*Math.sin(Math.PI),y: p.x*Math.sin(Math.PI)+p.y*Math.cos(Math.PI) }
    },
];

function muunnos(point){
     for(let i=0; i<pipeline.length; i++){   
        point = pipeline[i](point);
    }
    return point;
}
console.log(point);
console.log(muunnos(point));
pipeline[2](point)

//4
//const potenssi = (x,n) => n!=1?x*potenssi(x,n-1):x
const potenssi = (x,n,t=1) => n!=0?potenssi(x,n-1,t*x):t

//5
var Moduuli = function foo() {
  let x = 1;
  return {
  kasvata : function() { return ++x; },
  vanhenna : function() { return --x; }
}}()

