/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava4;

import java.util.Observable;
import tehtava4.teht5.Jugurtti;
import tehtava4.teht5.Juusto;
import tehtava4.teht5.Maito;
import tehtava4.teht5.MeijeriFactoryVahalaktoosinen;
import tehtava4.teht5.MeijeriFactoryLaktoositon;
import tehtava4.teht5.AbstractMeijeriFactory;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 *
 * @author Otso
 */
public class Tehtava4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Tehtävä 5
        AbstractMeijeriFactory tuoteTehdas = new MeijeriFactoryLaktoositon();
        Maito maito = (Maito) tuoteTehdas.getTuote("MAITO");
        Juusto juusto = (Juusto) tuoteTehdas.getTuote("JUUSTO");
        Jugurtti jugurtti = (Jugurtti) tuoteTehdas.getTuote("JUGURTTI");

        Consumer<String> printTuote = System.out::println;
        printTuote.accept(maito.getName());
        printTuote.accept(juusto.getName());
        printTuote.accept(jugurtti.getName());

        tuoteTehdas = new MeijeriFactoryVahalaktoosinen();
        maito = (Maito) tuoteTehdas.getTuote("MAITO");
        juusto = (Juusto) tuoteTehdas.getTuote("JUUSTO");
        jugurtti = (Jugurtti) tuoteTehdas.getTuote("JUGURTTI");

        printTuote.accept(maito.getName());
        printTuote.accept(juusto.getName());
        printTuote.accept(jugurtti.getName());

        //Tehtava 6
        UnaryOperator<String> ekaKasittelija
                = s -> s.replaceAll("[ ]{2,}", " ");

        UnaryOperator<String> tokaKasittelija
                = s -> s.replaceAll("[äå]", "a")
                .replaceAll("[ÄÅ]", "A")
                .replaceAll("[ö]", "o")
                .replaceAll("[Ö]", "O");

        UnaryOperator<String> kolmasKasittelija
                = s -> s.replaceAll("sturct", "struct");

        Function<String, String> ketju = ekaKasittelija.andThen(tokaKasittelija).andThen(kolmasKasittelija);

        String s = ketju.apply("Möi    ÅÅN sturct  ÄÄÖö!");
        System.out.println(s);

        //8
        Uutistoimisto u = new Uutistoimisto();
        Observable obs = new HelsinginSanomat();
        obs.addObserver((Observable o, Object arg)
                -> System.out.println((String) arg));

        ((HelsinginSanomat) obs).kasitteleUutinen(u.julkkisUutinen());
        ((HelsinginSanomat) obs).kasitteleUutinen(u.poliittinenUutinen());
        
        obs = new IltaSanomat();
        obs.addObserver((Observable o, Object arg)
                -> System.out.println((String) arg));

        ((IltaSanomat) obs).kasitteleUutinen(u.julkkisUutinen());
        ((IltaSanomat) obs).kasitteleUutinen(u.poliittinenUutinen());
        

    }

}
