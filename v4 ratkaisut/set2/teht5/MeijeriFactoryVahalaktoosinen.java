/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava4.teht5;

/**
 *
 * @author Otso
 */
public class MeijeriFactoryVahalaktoosinen extends AbstractMeijeriFactory {
    public MeijeriFactoryVahalaktoosinen(){
        map.put("MAITO",MaitoVahalaktoosinen::new);
        map.put("JUUSTO",JuustoVahalaktoosinen::new);
        map.put("JUGURTTI",JugurttiVahalaktoosinen::new);
    }
}
