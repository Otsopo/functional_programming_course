/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava4.teht5;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * @author Otso
 */
public abstract class AbstractMeijeriFactory {
    protected Map<String, Supplier<Maitotuote>> map; 
   
    public AbstractMeijeriFactory(){
        map = new HashMap<>();
    }   
        
    public Maitotuote getTuote(String tuoteTyyppi){
     Supplier<Maitotuote> tuote = map.get(tuoteTyyppi.toUpperCase());
     if(tuote != null) {
       return tuote.get();
     }
     throw new IllegalArgumentException("Ei tuotetta " + tuoteTyyppi.toUpperCase());
  }
}
