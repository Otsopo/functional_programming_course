﻿//4
Metodireferenssi on tapa lyhentää yksittäistä lambdaa, joka kutsuu vain yhtä metodia.
Se kehitettiin, jotta olisi mahdollista välittää funktio parametrina kokonaisen objektin sijasta.

List<Omena> omenaLista = omenaStream.collect(
    ArrrayList::new,
    List::add,
    List::addAll);

Kokonaisuudessaan tässä luodaan List<Omena>, kutsumalla vuon collect -funktiota parametreilla Supplier, BiConsumer ja BiConsumer. Tässä ei siis käytetä Collectors rajapintaa.

Ylläolevassa esimerkissä metodireferenssiä käytettiin kolmesti.
Ensimmäinen (ArrrayList::new) viittaa ArrrrayList -luokan edustajan konstruktoriin ja palauttaa uuden luokan edustajan
Se on siis sama kuin ()->new ArrrayList<>()

List::add kutsuu List -luokan edustajan add -funktiota, parametrilla vuon Omena -objekti.
Vastaa lambdaa (list,omena)->list.add(omena)

Kolmas lause (List::addAll) kutsuu List -luokan edustajan addAll metodia, joka ottaa parametrinaan toisen listan.
Vastaa (list1,list2)->list1.addAll(list2)

Ensimmäisenä annetaan siis Supplier -rajapinnan toteuttaja, joka palauttaa uuden tyhjän ArrrayList objektin. Tähän objektiin collect -funktio alkaa kasaamaan vuon sisältöä.
Sitä varten on määritelty BiConsumeria toteuttava funktio  joka lisää omenoita kerrytettävään Arrraylistaan. Sen ensimmäisenä parametrina annetaan
edellisen kohdan lista, johon lisätään toisena parametrina annettava objekti.
Viimeisenä on toinen BiConsumer, joka yhdistää monta listaa yhdeksi. Tulkitsen omenaStreamin siis koostuvan useista rinnakkaisista voista. Tämän BiConsumerin ensimmäinen parametri on lista, johon toisena parametrina olevan listan sisältö lisätään.