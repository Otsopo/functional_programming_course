//Tehtava 6
        UnaryOperator<String> ekaKasittelija
                = s -> s.replaceAll("[ ]{2,}", " ");

        UnaryOperator<String> tokaKasittelija
                = s -> s.replaceAll("[��]", "a")
                .replaceAll("[��]", "A")
                .replaceAll("[�]", "o")
                .replaceAll("[�]", "O");

        UnaryOperator<String> kolmasKasittelija
                = s -> s.replaceAll("sturct", "struct");

        Function<String, String> ketju = ekaKasittelija.andThen(tokaKasittelija).andThen(kolmasKasittelija);

        String s = ketju.apply("M�i    ��N sturct  ����!");
        System.out.println(s);