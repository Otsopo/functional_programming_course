/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava4;

import java.util.HashMap;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 *
 * @author Otso
 */

public class Noppapeli {
private static class nPeli{
    HashMap<Integer,Integer> pisteet = new HashMap<>();
    int voittaja;
    
    public void makePlay(int pelaaja){
        int heitto =  new Random().nextInt(6)+1;
        pisteet.put(pelaaja,heitto);
    }
    
    public boolean endOfGame(){
        for(int i:pisteet.keySet()){
            if(pisteet.get(i)==6){
                voittaja = i;
                return true;
            }
        }
        return false;
    }
    
    public void printWinner(){
        System.out.println("Pelaaja "+voittaja+" on voittaja!");
    }
    
}
static class Game<T>{
    protected int playersCount;
    public final void pelaa(
    Supplier<T> initializeGame,
    BiConsumer<T,Integer> makePlay,
    Predicate<T> endOfGame,
    Consumer<T> printWinner){
        
        this.playersCount = playersCount;
        T peli = initializeGame.get();
        int j = 0;
        while (!endOfGame.test(peli)){
            makePlay.accept(peli,j);
            j = (j + 1) % playersCount;
        }
        printWinner.accept(peli);
        
    }
        
}    

    public static final void playOneGame(int playersCount) {
        Game<nPeli> noppapeli = new Game<>();
        noppapeli.playersCount = 2;
        noppapeli.pelaa(
                nPeli::new,
                nPeli::makePlay,
                (p->p.endOfGame()),
                nPeli::printWinner);
        
    }
    
    public static void main(String args[]){
        playOneGame(2);
    }
}
