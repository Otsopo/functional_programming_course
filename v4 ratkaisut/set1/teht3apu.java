/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.LongAccumulator;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 *
 * @author Otso
 */
public class Tehtava3Funktiot {

    

    public static long linkedList(long n) {
        LongAccumulator accumulator = new LongAccumulator((acc, x) -> acc + x, 0);
        LongStream.rangeClosed(1, n).boxed().collect(toCollection(LinkedList::new)).parallelStream()
                    .map(x -> x * x)
                    .forEach(accumulator::accumulate);
        return accumulator.longValue();
    }

    public static long arrayList(long n) {
        LongAccumulator accumulator = new LongAccumulator((acc, x) -> acc + x, 0);
        List<Long> al = LongStream.rangeClosed(1, n).boxed().collect(toList());
        al = new ArrayList<>(al);
        al.parallelStream()
                    .map(x -> x * x)
                    .forEach(accumulator::accumulate);
        return accumulator.longValue();
    }
    public static long intStream(long n) {
        LongAccumulator accumulator = new LongAccumulator((acc, x) -> acc + x, 0);
        IntStream.rangeClosed(1,(int)n).parallel().map(x -> x * x).forEach(accumulator::accumulate);
        return accumulator.longValue();
    }
    
}
