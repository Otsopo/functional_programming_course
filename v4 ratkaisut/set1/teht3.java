/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava3;

import java.util.function.Function;


/**
 *
 * @author Otso
 */
public class ParallelTehtava3 {

    public static void main(String[] args) {
        System.out.println("LinkedList done in: " + measurePerf(Tehtava3Funktiot::linkedList, 10_000_00L) + " msecs");
        System.out.println("ArrayList done in: " + measurePerf(Tehtava3Funktiot::arrayList, 10_000_00L) + " msecs");
        System.out.println("IntStream done in: " + measurePerf(Tehtava3Funktiot::intStream, 10_000_00L) + " msecs");
        

    }
    
public static <T, R> long measurePerf(Function<T, R> f, T input) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            R result = f.apply(input);
            long duration = (System.nanoTime() - start) / 1_000_000;
            System.out.println("Result: " + result);
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
}
