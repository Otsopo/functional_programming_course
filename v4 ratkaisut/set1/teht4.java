/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava3;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author Otso
 */
public class kalevalaparallel {

    public static void main(String args[]) {
        IntSupplier fib = new IntSupplier() {
            private int previous = 0;
            private int current = 1;

            public int getAsInt() {
                int nextValue = this.previous + this.current;
                this.previous = this.current;
                this.current = nextValue;
                return this.previous;
            }
        };
        IntStream.generate(fib).limit(10).forEach(i -> System.out.println());
        Path dir = Paths.get("F:\\funpro\\v3\\01\\JavaApp");

        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            kalevalaNormal(dir);
            long duration = (System.nanoTime() - start) / 1_000_000;
            if (duration < fastest) {
                fastest = duration;
            }
        }
        System.out.println("Normal: " + fastest + " ms");

        fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            kalevalaParallel(dir);
            long duration = (System.nanoTime() - start) / 1_000_000;
            if (duration < fastest) {
                fastest = duration;
            }
        }
        System.out.println("Parallel: " + fastest + " ms");

    }

    private static void kalevalaNormal(Path dir) {
        try {
            Map<String, Long> uniqueWords = Files.lines(dir.resolve("kalevala.txt"), Charset.defaultCharset())
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .map(s -> s.toLowerCase()
                            .replace(".", "")
                            .replace(",", "")
                            .replace(";", "")
                            .replace("?", "")
                            .replace("!", "")
                    ).collect(
                            Collectors.groupingBy(Function.identity(), Collectors.counting())
                    );
            //{=3, tieohesta=1, lyökämme=1, opetti=1, nyt=1, kanssa=1, sormet=1, lähteäni=1, raukoilla=1, kalevalan=1, kultaisilla=1, hyviä=1, tietä=1, parahia=1, puuttunut=1, poloisilla=1, lemminkäinen=1, pyöriessä=1, yhyttyämme=1, veikkoseni=1, kätehen=1, kirjavan=1, laulamahan=3, katkomia=1, ongelmoita=1, katoi=1, riipomia=1, luottehisin=1, keralla=1, vipunen=1, noien=1, kulta=1, kera=1, ilmarisen=1, harvoin=1, rajoilla=1, sulavat=1, niitä=1, pohjan=2, pikkaraisna=1, mieleni=1, kimmon=1, mustan=1, eikä=1, toisihimme=1, saamia=1, heinän=1, yhymme=1, kultaisien=1, hieromia=1, joukahaisen=1, minun=2, eessä=1, oppimia:=1, veli=1, puhe'et=1, lattialla=1, polven=1, muitaki=1, vetelemiä=1, käyessäni=1, kahta'alta=1, lomahan=1, suoltamahan=1, vanhan=1, nuorisossa=1, tempomia=1, jälessä=1, noita=1, mättähillä=1, ajattelevi=1, periltä=1, äitini=1, väätessänsä=1, viel'=1, lauloi=1, vuollessansa=1, nousevassa=1, kuulla=1, kansassa=1, sormien=1, mielitehtoisien=1, ennen=1, yhtehen=2, sanat=1, virsiä=1, suussani=1, kuoli=1, sampo=2, risukoista=1, metisillä=1, vyöltä=1, ei=1, päästä=2, kasvinkumppalini=1, kalvan=1, hajoovat=1, kunnahilla=1, luottehia:=1, paimenessa=1, lajivirttä=1, putoelevat=1, muurikin=1, toinen=1, jousen=1, peltojen=1, maitopartana=1, leikkilöihin=1, raitiolta=1, lähe=1, sanoja=3, väinämöisen=1, lauloaksemme=1, sukuvirttä=1, ahjon=1, kankahilta=1, pahaisna=1, vesoista=1, tekevi=1, piimäsuuna=1, kanervoista=1, lasna=2, virittämiä=1, ratkomia=1, saa=1, kirvesvartta=1, saamme=1, aivoni=1, kielelleni=1, käytyämme=1, pannaksemme=1, kasuavassa:=1, käsi=1, värttinätä=1, on=1, näillä=1, mailla=1, kerkiävät=1, hampahilleni=1, karjanlaitumilla=1, saa'ani=1, sanelemahan=2, alta=1, kaukomielen=1, louhi=2, sanoihin=1, tiestä=1, kaunis=1, isoni=1, niit'=1, vanheni=1, virsihin=1}
            System.out.println("5: " + uniqueWords.toString());
        } catch (Exception e) {
            System.out.println("5: Aseta kalevala.txt oikeaan tiedostoon");
        }
    }

    private static void kalevalaParallel(Path dir) {
        try {
            Map<String, Long> uniqueWords = Files.lines(dir.resolve("kalevala.txt"), Charset.defaultCharset())
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .parallel()
                    .map(s -> s.toLowerCase()
                            .replace(".", "")
                            .replace(",", "")
                            .replace(";", "")
                            .replace("?", "")
                            .replace("!", "")
                    ).collect(
                            Collectors.groupingBy(Function.identity(), Collectors.counting())
                    );
            //{=3, tieohesta=1, lyökämme=1, opetti=1, nyt=1, kanssa=1, sormet=1, lähteäni=1, raukoilla=1, kalevalan=1, kultaisilla=1, hyviä=1, tietä=1, parahia=1, puuttunut=1, poloisilla=1, lemminkäinen=1, pyöriessä=1, yhyttyämme=1, veikkoseni=1, kätehen=1, kirjavan=1, laulamahan=3, katkomia=1, ongelmoita=1, katoi=1, riipomia=1, luottehisin=1, keralla=1, vipunen=1, noien=1, kulta=1, kera=1, ilmarisen=1, harvoin=1, rajoilla=1, sulavat=1, niitä=1, pohjan=2, pikkaraisna=1, mieleni=1, kimmon=1, mustan=1, eikä=1, toisihimme=1, saamia=1, heinän=1, yhymme=1, kultaisien=1, hieromia=1, joukahaisen=1, minun=2, eessä=1, oppimia:=1, veli=1, puhe'et=1, lattialla=1, polven=1, muitaki=1, vetelemiä=1, käyessäni=1, kahta'alta=1, lomahan=1, suoltamahan=1, vanhan=1, nuorisossa=1, tempomia=1, jälessä=1, noita=1, mättähillä=1, ajattelevi=1, periltä=1, äitini=1, väätessänsä=1, viel'=1, lauloi=1, vuollessansa=1, nousevassa=1, kuulla=1, kansassa=1, sormien=1, mielitehtoisien=1, ennen=1, yhtehen=2, sanat=1, virsiä=1, suussani=1, kuoli=1, sampo=2, risukoista=1, metisillä=1, vyöltä=1, ei=1, päästä=2, kasvinkumppalini=1, kalvan=1, hajoovat=1, kunnahilla=1, luottehia:=1, paimenessa=1, lajivirttä=1, putoelevat=1, muurikin=1, toinen=1, jousen=1, peltojen=1, maitopartana=1, leikkilöihin=1, raitiolta=1, lähe=1, sanoja=3, väinämöisen=1, lauloaksemme=1, sukuvirttä=1, ahjon=1, kankahilta=1, pahaisna=1, vesoista=1, tekevi=1, piimäsuuna=1, kanervoista=1, lasna=2, virittämiä=1, ratkomia=1, saa=1, kirvesvartta=1, saamme=1, aivoni=1, kielelleni=1, käytyämme=1, pannaksemme=1, kasuavassa:=1, käsi=1, värttinätä=1, on=1, näillä=1, mailla=1, kerkiävät=1, hampahilleni=1, karjanlaitumilla=1, saa'ani=1, sanelemahan=2, alta=1, kaukomielen=1, louhi=2, sanoihin=1, tiestä=1, kaunis=1, isoni=1, niit'=1, vanheni=1, virsihin=1}
            System.out.println("5: " + uniqueWords.toString());
        } catch (Exception e) {
            System.out.println("5: Aseta kalevala.txt oikeaan tiedostoon");
        }
    }
}
