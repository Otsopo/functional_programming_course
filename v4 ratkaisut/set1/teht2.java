/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava3;

/**
 *
 * @author Otso
 */
import java.util.stream.IntStream;

public class StreamFilterMapReduce {

    public static void main(String[] args) {
        //  sum of the triples of even integers from 2 to 10
        System.out.printf(
                "Sum of the triples of even integers from 2 to 10 is: %d%n",
                IntStream.rangeClosed(1, 10)
                .peek(x -> System.out.println("Filter: " + x)) // b) 10 kertaa
                .filter(x -> x % 2 == 0)
                .peek(i -> System.out.println("Map: " + i))// a) 5 kertaa
                .map(x -> x * 3)
                .sum());
        System.out.println("=======");
        IntStream.rangeClosed(1, 10)
                .peek(i -> System.out.println("Map: " + i))// c) 10 kertaa
                .map(x -> x * 3)
                .peek(x -> System.out.println("Filter: " + x)) //(10 kertaa)
                .filter(x -> x % 2 == 0)                
                .sum();
  }
}
