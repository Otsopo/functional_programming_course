package testing;

import java.util.*;
import static java.util.stream.Collectors.toList;
import org.junit.*;
import static org.junit.Assert.*;


public class Debugging {

    public static void main(String[] args) {
        List<Point> points = Arrays.asList(new Point(12, 2), null);
        points.stream().map(p -> p!=null?p.getX():null).forEach(System.out::println);

    }

    private static class Point {

        private int x;
        private int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public Point moveRightBy(int xx) {
            return new Point(x + xx, y);
        }

        public static List<Point> moveAllPointsRightBy(List<Point> points, int x) {
            return points.stream()
                    .parallel()
                    .map(p -> p!=null?p.moveRightBy(x):null)
                    .collect(toList());
        }
    }
    
    @Test
    public void testmoveAllPointsRightBy()
            throws Exception {
        List<Point> points = Arrays.asList(new Point(5, 5), new Point(10, 5));
        List<Point> expectedPoints
                = Arrays.asList(new Point(15, 5),
                        new Point(20, 5));
        List<Point> newPoints = Point.
                moveAllPointsRightBy(points, 10);
        for(int i = 0; i<expectedPoints.size();i++){
           assert(expectedPoints.get(i).x == newPoints.get(i).x);
        }
    }
}
