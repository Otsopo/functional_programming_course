//1
const toCelsius = fahrenheit => (5/9)*fahrenheit-32
const area = radius => Math.PI*radius*radius
//2 https://stackoverflow.com/questions/17781472/how-to-get-a-subset-of-a-javascript-objects-properties
const mapMovies = (({title,release}) =>  ({title,release}))
//3
const filterMovies = ((movie) => movie.release>2011)
//4
const hLampo15 = [-20,-17,-7,3,15,15,19,27,13,14,4,0]
const hLampo16 = [-10,-12,-7,3,15,15,9,22,13,12,4,-1]

let avgs = hLampo15.map(((value,index)=>(hLampo15[index] + Lampo16[index])/2)).filter((lampo)=>lampo>0)
avgs = avgs.reduce((acc,curr)=>acc+curr)/avgs.length
//5
const kalevala = "Mieleni minun tekevi, aivoni ajattelevi lähteäni laulamahan, saa'ani sanelemahan, sukuvirttä suoltamahan, lajivirttä laulamahan. Sanat suussani sulavat, puhe'et putoelevat, kielelleni kerkiävät, hampahilleni hajoovat.Veli kulta, veikkoseni, kaunis kasvinkumppalini! Lähe nyt kanssa laulamahan, saa kera sanelemahan yhtehen yhyttyämme, kahta'alta käytyämme! Harvoin yhtehen yhymme, saamme toinen toisihimme näillä raukoilla rajoilla, poloisilla Pohjan mailla.Lyökämme käsi kätehen, sormet sormien lomahan, lauloaksemme hyviä, parahia pannaksemme, kuulla noien kultaisien, tietä mielitehtoisien, nuorisossa nousevassa, kansassa kasuavassa: noita saamia sanoja, virsiä virittämiä vyöltä vanhan Väinämöisen, alta ahjon Ilmarisen, päästä kalvan Kaukomielen, Joukahaisen jousen tiestä, Pohjan peltojen periltä, Kalevalan kankahilta.Niit' ennen isoni lauloi kirvesvartta vuollessansa; niitä äitini opetti väätessänsä värttinätä, minun lasna lattialla eessä polven pyöriessä, maitopartana pahaisna, piimäsuuna pikkaraisna. Sampo ei puuttunut sanoja eikä Louhi luottehia: vanheni sanoihin sampo, katoi Louhi luottehisin, virsihin Vipunen kuoli, Lemminkäinen leikkilöihin.Viel' on muitaki sanoja, ongelmoita oppimia: tieohesta tempomia, kanervoista katkomia, risukoista riipomia, vesoista vetelemiä, päästä heinän hieromia, raitiolta ratkomia, paimenessa käyessäni, lasna karjanlaitumilla, metisillä mättähillä, kultaisilla kunnahilla, mustan Muurikin jälessä, Kimmon kirjavan keralla."

let sliced = kalevala.toLowerCase().split('.').join(" ")
sliced = sliced.split(',').join("")
sliced = sliced.split(" ")

var tokenize = {}
sliced.map((sana=>tokenize[sana]!=null?tokenize[sana]++:tokenize[sana]=1))