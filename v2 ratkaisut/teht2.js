
//1
const maki = (k,lp)=>((m)=>{return 60+(m-k)*lp})
const normaalimaki = maki(75,2)
const suurimaki = maki(100,1.8)
//2
const auto = (()=>{
  const suojatutMuuttujat = new WeakMap()
  class auto{
    constructor(a_tankki,a_mmittari){
        suojatutMuuttujat.set(this,{matkamittari : a_mmittari})
        this.tankki = a_tankki
      }
    getMMittari(){
    return suojatutMuuttujat.get(this).matkamittari
    }
    getTankki(){
      return this.tankki
    }
    lisaaPolttoainetta(maara){
      this.tankki += maara
    }
    aja(){
      this.tankki-=1
      let mittari =  this.getMMittari()
      mittari += 10
      suojatutMuuttujat.set(this,{matkamittari : mittari})
      console.log(this.getTankki(),this.getMMittari())
    }
  }
  return auto
})()
//3
function autoIVO(tankki,MM){
  let _tankki = tankki
  let _MM = MM
  return{
    aja:function(){
      tankki = _tankki -1
      MM = _MM + 10
      console.log(tankki,MM)
    return new autoIVO(tankki,MM);
    },
    lisaaPolttoainetta: function(maara){
      return _tankki + maara
    }
  }
}
const immutableAuto = autoIVO(10,50)

//4
const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen'])
const set2 = set1.add('ruskea')
//set1 === set2 palauttaa false
const set3 = set2.add('ruskea')
//Set voi sisältää vain uniikkeja arvoja, joten set2===set3 palauttaa true. 

//5
//Range on laiska, joten käytän sitä.
//Tein vain mielikuvituksettoman funktion joka demonstroi laiskaa evaluaatiota. 
let ar = Immutable.Range(1000000000000000000)

//Filteriä ja mappeja kutsutaan vain kymmenen kertaa, eli ei käydä jokaista alkiota läpi. Vaikka filter ja take ovat molemmat viimeisiä funktiokutsuja, ne silti suoritetaan ensimmäisenä laiskan toteutuksen takia.
const lazyFunction = (y)=>(ar.map(x=>-x)
            .map(x=>x*y)
            .filter(x=>(x%5)==0)
            .take(10)
            .reduce((acc,x)=>acc+x))