/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat2;

import java.util.function.IntSupplier;
import java.util.stream.IntStream;

/**
 *
 * @author Otso
 */
public class Fibonacci {
    
    public static void main(String[] args) {
        IntSupplier  fibo = new IntSupplier(){
        int first = 0;
        int second = 0;
        
        @Override
        public int getAsInt() {
            if(second==0){
                second = 1;
                return 1;
            }
            int next = first + second;
            first = second;
            second = next;
            return next;
        }
        
    };
    IntStream.generate(fibo).limit(10).forEach((n) -> System.out.println(n));

    }
    
}
