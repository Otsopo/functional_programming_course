/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtavat2;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;

/**
 *
 * @author Otso
 */

public class Lottonumero {
    //https://stackoverflow.com/questions/20058366/shuffle-a-list-of-integers-with-java-8-streams-api
    private static final Collector<?, ?, ?> SHUFFLER = Collectors.collectingAndThen(
        Collectors.toList(),
        list -> {
            Collections.shuffle(list);
            return list;
        }
);

    @SuppressWarnings("unchecked")
    public static <T> Collector<T, ?, List<T>> toShuffledList() {
        return (Collector<T, ?, List<T>>) SHUFFLER;
    }
    static IntStream arpoja(){
                return IntStream
                    .range(1, 31)
                    .boxed()
                    .collect(toShuffledList())
                    .stream()
                    .mapToInt(Integer::intValue)
                    .limit(7);
   }

    public static void main(String[] args) {


        IntStream numerot = arpoja();
        numerot.forEach(number->System.out.println(number));
        
        lottoFIF numerot2 = new lottoFIF() {
            @Override
            public IntStream getNumerot() {
                return IntStream
                    .range(1, 31)
                    .boxed()
                    .collect(toShuffledList())
                    .stream().mapToInt(Integer::intValue).limit(7);
            }            
        };
        System.out.println("----");

        numerot2.getNumerot().forEach(number->System.out.println(number));


    }

    

    
}
