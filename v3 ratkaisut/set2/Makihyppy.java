import java.util.function.DoubleUnaryOperator;

public class Makihyppy {

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
            return (m)->60+(m-kPiste)*lisapisteet;
    }
        
    public static void main(String[] args) {

       
       DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 1.8);
       
       System.out.println(normaaliLahti.applyAsDouble(90)); 
          
    }
    
}