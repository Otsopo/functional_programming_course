package java2_OmaFIF;


import java.util.function.IntSupplier;
import java2_OmaFIF.Tulostaja;

class Functional2{
    
    // Hyödynnetään itse määriteltyä GeneratorFIF-rajapintaa
    // ja Tulostaja-luokkaa


    public static void main(String[] args){
    
        // GeneratorFIF:n toteutuksia:
    
        IntSupplier generatorOldWay = new IntSupplier(){
            public int getAsInt(){
                return 3;
            }
        };
        
        // Koska kyseessä funktionaalinen rajapinta, voidaan hyödyntää lambda-lausekkeita:
        
        IntSupplier generaattori1 = () -> 2;  
        IntSupplier generaattori2 = () -> (int)(Math.random() * 6 + 1);  
        
        
        Tulostaja t = new Tulostaja();
        
        t.tulosta(generatorOldWay);
        t.tulosta(generaattori1);
        t.tulosta(generaattori2);
        t.tulosta(()->100);
    }   
    
}


