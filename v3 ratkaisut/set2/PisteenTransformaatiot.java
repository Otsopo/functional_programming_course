import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class PisteenTransformaatiot {
          
    public static void main(String[] args) {
               
       Function siirto = Piste.makeSiirto(1, 2);
       Function skaalaus = Piste.makeSkaalaus(2);
       Function kierto = Piste.makeKierto(Math.PI);
       Function muunnos = siirto.andThen(skaalaus).andThen(kierto);
       
       Piste[] pisteet = {new Piste(1,1), new Piste(2,2), new Piste(3,3)};
       List<Piste> uudetPisteet = new CopyOnWriteArrayList();
       
       for (Piste p: pisteet){
           uudetPisteet.add((Piste)muunnos.apply(p));
       } 
  
       uudetPisteet.forEach(p -> System.out.println(p));
    }

    private static class Piste {
        double x = 0;
        double y = 0;
        public static Function<Piste,Piste> makeSiirto(double dx,double dy){
            return (piste)->{
                piste.x += dx;
                piste.y += dy;
                return piste;
            };
        }
        public static Function<Piste,Piste> makeSkaalaus(double skaala){
            return (piste)->{
                piste.x = piste.x * skaala;
                piste.y = piste.y * skaala;
                return piste;
            };
        }
        public static Function<Piste,Piste> makeKierto(double kierto){
            return (piste)->{
                piste.x = (piste.x*Math.cos(kierto) - piste.y*Math.sin(kierto));
                piste.y=(piste.x*Math.sin(kierto) + piste.y*Math.cos(kierto));
                return piste;
            };
        }
        public Piste() {
        }
         public Piste(double x,double y) {
             this.x = x;
             this.y = y;
        }
        @Override
        public String toString(){
            return x + " " + y;
        }
    }
}