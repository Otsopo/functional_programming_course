;1  [rows (Integer. (get-input 5)) board (if (< rows 5) (prompt-rows) (new-board rows))]

;2
;(new-board 5) =>
;    {
;    7 {:connections {2 4, 9 8}, :pegged true},
;    1 {:pegged true, :connections {4 2, 6 3}},
;    4 {:connections {1 2, 6 5, 11 7, 13 8}, :pegged true},
;    15 {:connections {6 10, 13 14}, :pegged true},
;    13 {:connections {4 8, 6 9, 11 12, 15 14}, :pegged true},
;    :rows 5, 6 {:connections {1 3, 4 5, 13 9, 15 10}, :pegged true},
;    3 {:pegged true, :connections {8 5, 10 6}},
;    12 {:connections {5 8, 14 13}, :pegged true},
;    2 {:pegged true, :connections {7 4, 9 5}},
;    11 {:connections {4 7, 13 12}, :pegged true}, 
;    9 {:connections {2 5, 7 8}, :pegged true}, 
;    5 {:pegged true, :connections {12 8, 14 9}}, 
;    14 {:connections {5 9, 12 13}, :pegged true}, 
;    10 {:connections {3 6, 8 9}, :pegged true}, 
;    8 {:connections {3 5, 10 9}, :pegged true}
;    }
; Kyseess� on nested map, jonka jokaisen pegpaikan avain on sen numero, ja arvona toinen map, joka kertoo :connectiot ja :pegged arvon.

;3 
;    (take 6 tri): tri on m��ritelty (def tri (tri*)), eli se sis�lt�� loputtoman m��r�n tri* funktion generoimia numeroita lazy seq muodossa.
;    Take 6 ottaa n�ist� 6 ensimm�isest� elementist� koostuvan sequensin eli (1 3 6 10 15 21)

;   (last (take 6 tri)) last on seq funktio joka palauttaa sen viimeisen elementin, eli 21. row-tri -funktiossa t�t� hy�dynnet��n rivin maksimipegim��r�n hakemiseen.

;4
;   Ensimm�inen funktio perustuu siihen, ett� haetaan boardista pos -muuttujaa vastaava peg, ja sen :pegged arvo.
;   Esimerkiksi (get-in board [13 :pegged])) palauttaisi true jos boardista l�ytyisi seuraava rivi (13 {:connections {4 8, 6 9, 11 12, 15 14}, :pegged true})
;   
;   Seuraavassa kahdessa funktiossa k�ytet��n (assoc-in m [k & ks] v) -funktiota, jolla voidaan korvata sis�kk�isen assosiatiivisen rakenteen arvoja toisilla arvoilla.
;   T�ss� tapauksessa tehd��n halutusta :pegged arvosta joko true tai false.
;   
;   Viimeinen funktio k�ytt�� remove ja place-peg funktioita pegin liikuttamiseen. K�yt�nn�ss� siis peliss� ei liikuteta pegej�, vaan muutetaan l�ht�paikan :pegged falseksi ja kohteen arvo trueksi.
;
;   Kokeilin valid-movesia:
;   Tulostin ensin pelilautani mappina ja etsin kaikki paikat, joiden connectioneihin lukeutui 4, joka oli tyhj�.
;   N�ille paikoille valid-moves palautti mahdollisia siirtoja, mutta muille se ei l�yt�nyt yht�k��n.

;5
;   valid-move? kertoo onko hyppy paikasta 1 paikkaan 2 sallittu.
;   Se etsii ensimm�isen paikan valid-movesin palauttamista arvoista toista paikkaa, ja palauttaa v�liin j��v�n paikan.
;   Hyppy on k�yt�nn�ss� sallittu vain jos hypp�� jonkun paikan yli.

;   make-move tarkistaa hyp�t��nk� pegin yli, ja kutsuu move-peg funktiota
;   pelilaudalla, josta on poistettu v�liin j��v� peg, sek� kahdella paikalla, joiden pegged arvo muutetaan.

;   can-move? kertoo voiko yksik��n peg liikkua.

;   (comp not-empty (partial valid-moves board)) yhdist�� ensin not-empty sek� osittain toteutetun valid-moves funktion.
;   (filter #(get (second %) :pegged) board) k�y jokaisen boardin paikan l�pi, ja karsii pois sellaiset, joissa pegged on false.
;   (map first (filter #(get (second %) :pegged) board)) ottaa jokaisesta boardin paikasta paikkanumeron,
;   ja antaa sen parametrina yhdistetylle comp funktiolle.
;   (some) tarkistaa palauttaako yhdistetty funktio yht�k��n truthy arvoa, eli onko yht�k��n liikutettavaa nappia.

;6 OK