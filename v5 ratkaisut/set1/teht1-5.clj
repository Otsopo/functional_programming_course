;1 ok
;2
(+ (* 2 5) 4)
(+ 1 2 3 4 5)

((fn [name] (str "Tervetuloa Tylypahkaan " name))"Otso")
;Tai
(def tyly(fn [name] (str "Tervetuloa Tylypahkaan " name)))
(tyly "otso")

;3
(get(get {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}}:name):middle)

;4
(def square(fn [x] (* x x)))
;5
(def karkausvuosi?
  (fn [v]
      (or
        (and (= (mod v 4) 0) (not (= (mod v 100) 0) ))
        (= (mod v 400) 0)
        )
      )
    )
    